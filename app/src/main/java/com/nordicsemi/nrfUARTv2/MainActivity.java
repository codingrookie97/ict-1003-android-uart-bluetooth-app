
/*
 * Copyright (c) 2015, Nordic Semiconductor
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.nordicsemi.nrfUARTv2;




import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;


import com.nordicsemi.nrfUARTv2.UartService;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements RadioGroup.OnCheckedChangeListener {
    private static final int REQUEST_SELECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    private static final int UART_PROFILE_READY = 10;
    public static final String TAG = "nRFUART";
    private static final int UART_PROFILE_CONNECTED = 20;
    private static final int UART_PROFILE_DISCONNECTED = 21;
    private static final int STATE_OFF = 10;

    //TextView mRemoteRssiVal;
    //RadioGroup mRg;
    private int mState = UART_PROFILE_DISCONNECTED;
    private boolean onConnected = false;
    private UartService mService = null;
    private BluetoothDevice mDevice = null;
    private List<BluetoothDevice> deviceList = new ArrayList<BluetoothDevice>();
    private BluetoothAdapter mBtAdapter = null;
    private int qChoice = 1;
    private ListView messageListView;
    private MediaPlayer backgroundMusic;
    private ArrayAdapter<String> listAdapter;
    private AlertDialog promptQuiz, quizSelections;
    private Button btnBegin, btnQuit;
    private TextView txtUsername, txtTitle, qnsTitle, ansA, ansB, ansC, ansD, timerText;
    private List<Player> playerList = new ArrayList<Player>();
    private int timerDuration = 10;
    //private EditText edtMessage;

    public class Question{
        public String questionText;
        public List<String> answers = new ArrayList<String>();
        public int correctAnswer;
    }

    public class Player{
        public int score = 0;
        public Boolean answered = false;
        public String name = "User 1";
    }

    public static List<Question> questionList = new ArrayList<Question>();
    private int currentQuestion = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_page);
        mBtAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBtAdapter == null) {
            Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        //messageListView = (ListView) findViewById(R.id.listMessage);
        listAdapter = new ArrayAdapter<String>(this, R.layout.message_detail);
        //messageListView.setAdapter(listAdapter);
        //messageListView.setDivider(null);
        backgroundMusic = MediaPlayer.create(MainActivity.this, R.raw.backgroundmusic);
        backgroundMusic.setLooping(true);
        backgroundMusic.start();
        btnBegin = (Button) findViewById(R.id.btnBegin);
        btnQuit = (Button) findViewById(R.id.btnQuit);
        txtUsername = (TextView) findViewById(R.id.txtUsername);
        //btnSend=(Button) findViewById(R.id.sendButton);
        //edtMessage = (EditText) findViewById(R.id.sendText);
        service_init();
        Player newPlayer = new Player();
        playerList.add(newPlayer);
        txtUsername.setText(getString(R.string.user) + " " + playerList.get(0).name);

        btnQuit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.exit(0);
            }
        });

        btnBegin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mBtAdapter.isEnabled()) {
                    Log.i(TAG, "onClick - BT not enabled yet");
                    Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
                }
                else {
                    Intent newIntent = new Intent(MainActivity.this, DeviceListActivity.class);
                    startActivityForResult(newIntent, REQUEST_SELECT_DEVICE);
                    if (mDevice!=null)
                    {
                        mService.disconnect();

                    }
                }
            }
        });
    }
    
    //UART service connected/disconnected
    private ServiceConnection mServiceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder rawBinder) {
        		mService = ((UartService.LocalBinder) rawBinder).getService();
        		Log.d(TAG, "onServiceConnected mService= " + mService);
        		if (!mService.initialize()) {
                    Log.e(TAG, "Unable to initialize Bluetooth");
                    finish();
                }

        }

        public void onServiceDisconnected(ComponentName classname) {
       ////     mService.disconnect(mDevice);
        		mService = null;
        }
    };

    private Handler mHandler = new Handler() {
        @Override
        
        //Handler events that received from UART service 
        public void handleMessage(Message msg) {
  
        }
    };

    private final BroadcastReceiver UARTStatusChangeReceiver = new BroadcastReceiver() {

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            final Intent mIntent = intent;
           //*********************//
            if (action.equals(UartService.ACTION_GATT_CONNECTED)) {
                if (promptQuiz == null ) {
                    displaySuccessfulPopup(mDevice.getName());
                } else if (promptQuiz != null && !promptQuiz.isShowing()) {
                    displaySuccessfulPopup(mDevice.getName());
                }
            	 runOnUiThread(new Runnable() {
                     public void run() {
                         	String currentDateTimeString = DateFormat.getTimeInstance().format(new Date());
                             Log.d(TAG, "UART_CONNECT_MSG");
                             ((TextView) findViewById(R.id.deviceName)).setText(mDevice.getName()+ " - ready");
                             mState = UART_PROFILE_CONNECTED;
                         onConnected = true;
                     }
            	 });
            }
           
          //*********************//
            if (action.equals(UartService.ACTION_GATT_DISCONNECTED)) {
            	 runOnUiThread(new Runnable() {
                     public void run() {
                    	 	 //String currentDateTimeString = DateFormat.getTimeInstance().format(new Date());
                         Log.d(TAG, "UART_DISCONNECT_MSG");
                             //edtMessage.setEnabled(false);
                             //btnSend.setEnabled(false);
                         onConnected = false;
                         currentQuestion = 0;
                         playerList.get(0).score = 0;
                         playerList.get(0).answered = false;
                         setContentView(R.layout.activity_main_page);
                         questionList.clear();
                         initMain();
                         //((TextView) findViewById(R.id.deviceName)).setText("Not Connected");
                         //listAdapter.add("["+currentDateTimeString+"] Disconnected to: "+ mDevice.getName());
                         showMessage("Sorry you have lost bluetooth connection, redirecting back to home page!");
                         mState = UART_PROFILE_DISCONNECTED;
                         mService.close();
                         //setUiState();
                         if (promptQuiz != null) {
                             promptQuiz.dismiss();
                         }
                         if (quizSelections != null) {
                             quizSelections.dismiss();
                         }
                     }
                 });
            }
            
          
          //*********************//
            if (action.equals(UartService.ACTION_GATT_SERVICES_DISCOVERED)) {
             	 mService.enableTXNotification();
            }
          //*********************//
            if (action.equals(UartService.ACTION_DATA_AVAILABLE)) {
              
                 final byte[] txValue = intent.getByteArrayExtra(UartService.EXTRA_DATA);
                 runOnUiThread(new Runnable() {
                     public void run() {
                         try {
                             String text = new String(txValue, "UTF-8");
                             if(!playerList.get(0).answered &&
                                     Character.getNumericValue(text.charAt(1)) == questionList.get(currentQuestion).correctAnswer){
                                 playerList.get(0).score++;
                                 playerList.get(0).answered = true;
                             }
                         	//String currentDateTimeString = DateFormat.getTimeInstance().format(new Date());
                        	 	//listAdapter.add("["+currentDateTimeString+"] RX: "+text);
                        	 	//messageListView.smoothScrollToPosition(listAdapter.getCount() - 1);
                        	
                         } catch (Exception e) {
                             Log.e(TAG, e.toString());
                         }
                     }
                 });
             }
           //*********************//
            if (action.equals(UartService.DEVICE_DOES_NOT_SUPPORT_UART)){
            	showMessage("Device doesn't support UART. Disconnecting");
            	mService.disconnect();
            }
            
            
        }
    };

    public void displaySuccessfulPopup(String deviceName) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Successfully connected to " + deviceName + "!");
        builder.setMessage("Are you ready to Fetch, Decode, Execute?");
        builder.setPositiveButton("Proceed To Quiz Selections", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                displayQuizSelections();
                dialog.dismiss();
                promptQuiz = null;
            }
        });

        builder.setNegativeButton("Nah, I'll see you next year!", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        promptQuiz = builder.create();
        promptQuiz.show();
    }

    public void displayQuizSelections() {
        String[] quizSet = {"Quiz Set 1", "Quiz Set 2"};
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Choose a Quiz Set");
        final int checkedItem = 0;
        builder.setSingleChoiceItems(quizSet, checkedItem, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int choice) {
                qChoice = choice + 1;
            }
        });

        builder.setPositiveButton("Proceed to Quiz", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                setContentView(R.layout.question);
                txtTitle = (TextView) findViewById(R.id.txtQuizTitle);
                txtTitle.setText("Quiz Set " + qChoice);
                try {
                    readQuestionFile(qChoice);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        quizSelections = builder.create();
        quizSelections.show();
    }

    private void readQuestionFile(int qChoice) throws IOException {
        //Create questions class: properties-question text(string), array of answers(string), correct answer(int)
        //Read from file_name
        //Create an array of questions, populate the questions using question class
        InputStream stream = getResources().openRawResource(getResources().getIdentifier("set" + qChoice + "_qns", "raw", getPackageName()));
        BufferedReader br = new BufferedReader(new InputStreamReader(stream));
        String line;
        Question q = null;
        while ((line = br.readLine()) != null) {
            if(line.charAt(0) == 'Q'){
                if(q != null)
                    questionList.add(q);
                q = new Question();
                q.questionText = line.substring(3);
                q.correctAnswer = Character.getNumericValue(line.charAt(2));
            }
            else{
                if(q != null){
                    q.answers.add(line);
                }
            }
            //System.out.println("Line entered : " + line);
        }
        if(q != null)
            questionList.add(q);

        stream.close();
        displayNextQuestion(questionList.get(currentQuestion));
    }

    private void displayNextQuestion(Question qArr){
        for(Player p : playerList){
            p.answered = false;
        }
        sendStringData("Q" + ((currentQuestion + 1) > 9 ? "" : "0") + (currentQuestion + 1));
        setContentView(R.layout.question);
        qnsTitle = (TextView) findViewById(R.id.qnsTitle);
        ansA = (TextView) findViewById(R.id.ansA);
        ansB = (TextView) findViewById(R.id.ansB);
        ansC = (TextView) findViewById(R.id.ansC);
        ansD = (TextView) findViewById(R.id.ansD);
        qnsTitle.setText(getString(R.string.question) + (currentQuestion + 1) + ")" + qArr.questionText);
        ansA.setText(getString(R.string.A) + " " + qArr.answers.get(0));
        ansB.setText(getString(R.string.B) + " " + qArr.answers.get(1));
        ansC.setText(getString(R.string.C) + " " + qArr.answers.get(2));
        ansD.setText(getString(R.string.D) + " " + qArr.answers.get(3));
        timerText = (TextView) findViewById(R.id.txtTimer);

        CountDownTimer t = new CountDownTimer(timerDuration * 1, 1000) {
            public void onTick(long millisUntilFinished) {
                timerText.setText("Time left: " + millisUntilFinished / 1000);
                if(!onConnected){
                    this.cancel();
                    return;
                }
            }

            public void onFinish() {
                if(!onConnected){
                    this.cancel();
                    return;
                }
                timerText.setText("Time's up!");
                onTimeUp();
            }
        };
        t.start();
    }

    private void onTimeUp(){
        setContentView(R.layout.times_up);
        Question current = questionList.get(currentQuestion);
        char correctChar = 'F';
        switch (current.correctAnswer)
        {
            case 1:
                correctChar = 'A';
                break;
            case 2:
                correctChar = 'B';
                break;
            case 3:
                correctChar = 'C';
                break;
            case 4:
                correctChar = 'D';
                break;
        }
        ((TextView)findViewById(R.id.txtCorrectAns)).setText(correctChar + ". " + current.answers.get(current.correctAnswer - 1));
        sendStringData("A" + current.correctAnswer);

        ((Button)findViewById(R.id.btnProceed)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(currentQuestion + 1 >= questionList.size()){
                    onGameOver();
                }
                else{
                    ++currentQuestion;
                    displayNextQuestion(questionList.get(currentQuestion));
                }
            }
        });
    }

    private void onGameOver(){
        sendStringData("S" + String.format("%03d", playerList.get(0).score) + String.format("%03d",questionList.size()) + "R01");
        setContentView(R.layout.end_screen);
        ImageView imgProf = (ImageView) findViewById(R.id.imgProf);
        TextView txtUsername = (TextView) findViewById(R.id.txtName);
        TextView txtRank = (TextView) findViewById(R.id.txtRank);
        TextView txtScore = (TextView) findViewById(R.id.txtScore);
        Button btnExit = (Button) findViewById(R.id.btnExit);
        ListView listView = (ListView)findViewById(R.id.listScore);
        LayoutInflater l = getLayoutInflater();
        ViewGroup myHeader = (ViewGroup)l.inflate(R.layout.list_header, listView, false);
        listView.addHeaderView(myHeader, null, false);
        List<Player> temp_list = new ArrayList<Player>();
        temp_list.add(playerList.get(0));
        Player p = new Player();
        p.score = 6;
        p.name = "User 2";
        temp_list.add(p);
        p = new Player();
        p.score = 5;
        p.name = "User 3";
        temp_list.add(p);
        Collections.sort(temp_list, new Comparator<Player>() {
            @Override
            public int compare(Player o1, Player o2) {
                return o2.score - o1.score;
            }
        });
        listView.setAdapter(new StudentList(this, temp_list));
        //If a user gets full marks, then display Professor Steven's Photo
        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentQuestion = 0;
                playerList.get(0).score = 0;
                playerList.get(0).answered = false;
                sendStringData("E");
                questionList.clear();
                setContentView(R.layout.activity_main_page);
                initMain();
            }
        });
        if (playerList.get(0).score == 10) {
            imgProf.setImageResource(R.drawable.prof_steven);
        } else {
            imgProf.setImageResource(R.drawable.prof_raymond);
        }
        txtUsername.setText(getString(R.string.user) + " User 1");
        txtRank.setText(getString(R.string.rank_title) + " 1st");
        txtScore.setText(getString(R.string.score_title) + " " + playerList.get(0).score + "/" + questionList.size());
    }

    private void initMain() {
        mBtAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBtAdapter == null) {
            Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        txtUsername = (TextView) findViewById(R.id.txtUsername);
        txtUsername.setText(getString(R.string.user) + " " + "User 1");
        service_init();
        Player newPlayer = new Player();
        playerList.add(newPlayer);
        btnBegin = (Button) findViewById(R.id.btnBegin);
        btnQuit = (Button) findViewById(R.id.btnQuit);
        btnQuit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.exit(0);
            }
        });

        btnBegin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mBtAdapter.isEnabled()) {
                    Log.i(TAG, "onClick - BT not enabled yet");
                    Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
                }
                else {
                    Intent newIntent = new Intent(MainActivity.this, DeviceListActivity.class);
                    startActivityForResult(newIntent, REQUEST_SELECT_DEVICE);
                    if (mDevice!=null)
                    {
                        mService.disconnect();

                    }
                }
            }
        });
    }

    private void sendStringData(String data){
        if (!onConnected) {
            return;
        }
        try {
            //send data to service
            byte[] value;
            value = data.getBytes("UTF-8");
            mService.writeRXCharacteristic(value);
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void service_init() {
        Intent bindIntent = new Intent(this, UartService.class);
        bindService(bindIntent, mServiceConnection, Context.BIND_AUTO_CREATE);
  
        LocalBroadcastManager.getInstance(this).registerReceiver(UARTStatusChangeReceiver, makeGattUpdateIntentFilter());
    }
    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(UartService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(UartService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(UartService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(UartService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(UartService.DEVICE_DOES_NOT_SUPPORT_UART);
        return intentFilter;
    }
    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDestroy() {
    	 super.onDestroy();
        Log.d(TAG, "onDestroy()");
        
        try {
        	LocalBroadcastManager.getInstance(this).unregisterReceiver(UARTStatusChangeReceiver);
        } catch (Exception ignore) {
            Log.e(TAG, ignore.toString());
        } 
        unbindService(mServiceConnection);
        mService.stopSelf();
        mService= null;
       
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "onStop");
        super.onStop();
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "onPause");
        super.onPause();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "onRestart");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        if (!mBtAdapter.isEnabled()) {
            Log.i(TAG, "onResume - BT not enabled yet");
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        }
 
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {

        case REQUEST_SELECT_DEVICE:
        	//When the DeviceListActivity return, with the selected device address
            if (resultCode == Activity.RESULT_OK && data != null) {
                String deviceAddress = data.getStringExtra(BluetoothDevice.EXTRA_DEVICE);
                BluetoothDevice foundDevice = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(deviceAddress);
                mDevice = foundDevice;
                deviceList.add(foundDevice);
               
                Log.d(TAG, "... onActivityResultdevice.address==" + mDevice + "mserviceValue" + mService);
                ((TextView) findViewById(R.id.deviceName)).setText(mDevice.getName()+ " - connecting");
                mService.connect(deviceAddress);
                            

            }
            break;
        case REQUEST_ENABLE_BT:
            // When the request to enable Bluetooth returns
            if (resultCode == Activity.RESULT_OK) {
                Toast.makeText(this, "Bluetooth has turned on ", Toast.LENGTH_SHORT).show();

            } else {
                // User did not enable Bluetooth or an error occurred
                Log.d(TAG, "BT not enabled");
                Toast.makeText(this, "Problem in BT Turning ON ", Toast.LENGTH_SHORT).show();
                finish();
            }
            break;
        default:
            Log.e(TAG, "wrong request code");
            break;
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
       
    }

    
    private void showMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        if (mState == UART_PROFILE_CONNECTED) {
            Intent startMain = new Intent(Intent.ACTION_MAIN);
            startMain.addCategory(Intent.CATEGORY_HOME);
            startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(startMain);
            showMessage("nRFUART's running in background.\n             Disconnect to exit");
        }
        else {
            new AlertDialog.Builder(this)
            .setIcon(android.R.drawable.ic_dialog_alert)
            .setTitle(R.string.popup_title)
            .setMessage(R.string.popup_message)
            .setPositiveButton(R.string.popup_yes, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
   	                finish();
                }
            })
            .setNegativeButton(R.string.popup_no, null)
            .show();
        }
    }
}

class StudentList extends BaseAdapter {

    Context context;
    List<MainActivity.Player> p;
    private static LayoutInflater inflater = null;

    public StudentList(Context context, List<MainActivity.Player> p) {
        this.context = context;
        this.context = context;
        this.p = p;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return p.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return p.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View vi = convertView;
        if (vi == null)
            vi = inflater.inflate(R.layout.list_row, null);
        TextView txtUserList = (TextView) vi.findViewById(R.id.txtUserList);
        TextView txtScoreList = (TextView) vi.findViewById(R.id.txtScoreList);
        TextView txtRankList = (TextView) vi.findViewById(R.id.txtRankList);
        txtUserList.setText(p.get(position).name);
        txtScoreList.setText(p.get(position).score + "/" + MainActivity.questionList.size());
        if (position == 0) {
            txtRankList.setText(position + 1 + "st");
        } else if (position == 1) {
            txtRankList.setText(position + 1 + "nd");
        } else if (position == 2){
            txtRankList.setText(position + 1 + "rd");
        }
        return vi;
    }
}